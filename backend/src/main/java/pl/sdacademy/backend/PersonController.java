package pl.sdacademy.backend;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet("/person")
public class PersonController extends HttpServlet {
    private List<Person> people = new ArrayList<>(Arrays.asList(
        new Person("Imie1", "Nazwisko1"),
        new Person("Imie2", "Nazwisko2"),
        new Person("Imie3", "Nazwisko3"),
        new Person("Imie4", "Nazwisko4")
    ));
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String json = objectMapper.writeValueAsString(people);
        response.getWriter().println(json);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Person person = objectMapper.readValue(request.getInputStream(), Person.class);
        people.add(person);
    }
}
