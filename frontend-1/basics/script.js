// System.out.prinln(...)
console.log("Hello!");
console.log(typeof 123);
console.log(typeof "abc");
console.log(typeof 'abc');
console.log(typeof `abc`);
console.log(typeof true);
console.log(typeof null);
console.log(typeof undefined);
console.log(typeof [3, 5, "abc"]);
console.log(typeof {a: 123});

const myArray = [34, "abc", true];
myArray.push("nowy element");
console.log(myArray);

myArray.forEach(element => console.log(element));

function add(a, b) {
    console.log("Wywołano funkcję add");
    console.log(arguments);
    return a + b;
}

console.log(add(5, 2));
console.log(add(5, 2, 10));
console.log(add(5));
console.log(add());

console.log(typeof add);
