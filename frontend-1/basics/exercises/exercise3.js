// JS.3 Stwórz skrypt, w którym zadeklarowana zostanie zmienna - przypisz jej tablicę wartości liczbowych.
// Następnie posortuj elementy tablicy za pomocą sortowania bąbelkowego (podejrzyj implementację Javową).
// Posortowaną tablicę wypisz w konsoli.
let array = [3, 2, 234, 324, 4566, 43, 23];
let n = array.length;

for (let i = 0; i < n - 1; i++)
    for (let j = 0; j < n - i - 1; j++)
        if (array[j] > array[j + 1]) {
            // swap temp and array[i]
            let temp = array[j];
            array[j] = array[j + 1];
            array[j + 1] = temp;
        }
console.log(array);
