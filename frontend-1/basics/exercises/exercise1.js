// JS.1 Stwórz skrypt, w którym zadeklarowane będą dwie zmienne. Zmiennym przypisz wartości liczbowe.
// Następnie spraw, aby w konsoli została wypisana wartość większej z nich.
let a = 12;
let b = 15;

// if (a > b) {
//     console.log(a);
// } else {
//     console.log(b);
// }
console.log(a > b ? a : b);
