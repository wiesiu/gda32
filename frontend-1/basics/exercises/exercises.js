// JS.4 Napisz funkcję js4, która przyjmie trzy argumenty - tablicę, liczbę oraz łańcuch znaków.
// Funkcja zwróci odpowiedź na pytanie, czy na wskazanym przez drugi argument miejscu w tablicy
// przekazanej jako pierwszy argument, znajduje się trzeci argument.
function js4(array, index, element) {
    if (!Array.isArray(array) || typeof index !== "number" || typeof element !== "string") {
        return;
    }
    // v1:
    // if (array[index] === element) {
    //     return true;
    // } else {
    //     return false;
    // }

    // v2:
    return array[index] === element;
}

// JS.5 Napisz funkcję js5, która przyjmie dwa argumenty.
// Pierwszy argument musi być tablicą (sprawdzamy to).
// Drugi argument jest opcjonalny - jeśli zostanie przekazany i będzie liczbą,
// to wypisujemy wartości pierwszego argumentu począwszy od indeksu równego drugiemu argumentowi.
// Jeśli drugi argument nie jest przekazany, to wypisujemy wszystkie elementy tablicy.
function js5(array, startIndex) {
    if (!Array.isArray(array)) {
        return;
    }
    if (typeof startIndex === "undefined") {
        startIndex = 0;
    }
    for (let i = startIndex; i < array.length; i++) {
        console.log(array[i]);
    }
}

console.log("js5([3, 2, 1, 543])");
js5([3, 2, 1, 543]);
console.log("js5([3, 2, 1, 543], 2)");
js5([3, 2, 1, 543], 2);

// function add(a, b) {
//     return a + b;
// }
// let add = function (a, b) {
//     return a + b;
// }
// let add = (a, b) => a + b;
