let myDiv = document.querySelector("#my-div");
console.log(myDiv);
console.dir(myDiv);
myDiv.innerHTML = "<p>Abcde</p>";
myDiv.style.backgroundColor = "blue";
myDiv.setAttribute("my-attribute", "my-value");
myDiv.classList.add("my-class");

myDiv.addEventListener("click", function (event) {
    console.log("Kliknięto element!");
    console.log("Szczegóły zdarzenia: ", event)
});

let userForm = document.querySelector("#userForm");
let firstNameInput = document.querySelector("#firstName");
let lastNameInput = document.querySelector("#lastName");

userForm.addEventListener("submit", function (event) {
    // Domyślnie po zatwierdzeniu formularza zostaje wysłane żądanie,
    // nie chcemy tego.
    event.preventDefault();
    console.log("Zatwierdzono formularz");
    let firstName = firstNameInput.value;
    let lastName = lastNameInput.value;
    console.log(`Imię: ${firstName}, nazwisko: ${lastName}`);
});

let newElement = document.createElement("a");
newElement.setAttribute("href", "https://google.com");
newElement.innerText = "Przekieruj do google.com";
newElement.style.color = "white";
myDiv.appendChild(newElement);
