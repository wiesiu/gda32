// FORM.2 Dołącz do dokumentu skrypt JS. Po kliknięciu przycisku "dodaj" powinien zostać utworzony obiekt
// z właściwościami zgodnymi z nazwami i wartościami pól. Obiekt powinien zostać wypisany w konsoli.
// FORM.3 Zadeklaruj zmienną people - tablicę (na starcie pustą). Po kliknięciu przycisku "dodaj" utworzony
// w zadaniu FORM.2 obiekt powinien zostać dodatkowo dodany do tablicy people.
// Form.4 funkcja obsługująca kliknięcie przycisku "dodaj" powinna sprawić, aby do tabeli trafił nowy wiersz,
// zawierający dane utworzonej osoby. Niech używa do tego funkcji refreshTable (którą sami stworzymy) - funkcji,
// która dla każdej osoby tworzy wiersz i wstawia go do tabeli.
let people = [];

let addUserForm = document.querySelector("#addUserForm");
let firstNameInput = document.querySelector("#firstName");
let lastNameInput = document.querySelector("#lastName");
let ageInput = document.querySelector("#age");
let tableBody = document.querySelector("tbody");

addUserForm.addEventListener("submit", function (event) {
    event.preventDefault();
    let firstName = firstNameInput.value;
    let lastName = lastNameInput.value;
    let age = ageInput.value;
    let person = {
        firstName: firstName,
        lastName: lastName,
        age: age,
    };
    // let person = {firstName, lastName, age};
    people.push(person);
    console.log(people);
    refreshTable();
});

function refreshTable() {
    tableBody.innerHTML = "";
    // v1:
    // for (let i = 0; i < people.length; i++) {
    //    ...
    // }
    // v2:
    people.forEach(person => {
        let rowElement = document.createElement("tr");
        rowElement.innerHTML = `
            <td>${person.firstName}</td>
            <td>${person.lastName}</td>
            <td>${person.age}</td>
            <td><input type="button" class="remove" value="Usuń"></td>`;
        tableBody.appendChild(rowElement);
        let button = rowElement.querySelector(".remove");
        button.addEventListener("click", function () {
            people = people.filter(p => p !== person);
            refreshTable();
        });
    });
}
