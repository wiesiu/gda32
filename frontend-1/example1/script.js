let personUrl = "http://localhost:8080/person";

let tableBody = document.querySelector("tbody");
let addUserForm = document.querySelector("#addUserForm");
let firstNameInput = document.querySelector("#firstName");
let lastNameInput = document.querySelector("#lastName");

addUserForm.addEventListener("submit", function (event) {
    event.preventDefault();
    let firstName = firstNameInput.value;
    let lastName = lastNameInput.value;
    let person = {
        firstName: firstName,
        lastName: lastName
    };
    fetch(personUrl, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(person)
    }).then(response => {
        if (response.ok) {
            refreshTable();
        }
    });
});

function fillTable(people) {
    tableBody.innerHTML = "";
    people.forEach(person => {
        let rowElement = document.createElement("tr");
        rowElement.innerHTML = `
            <td>${person.firstName}</td>
            <td>${person.lastName}</td>`;
        tableBody.appendChild(rowElement);
    });
}

function refreshTable() {
    fetch(personUrl)
        .then(response => response.json())
        .then(people => fillTable(people))
}

refreshTable();
